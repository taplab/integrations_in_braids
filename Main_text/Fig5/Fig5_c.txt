#First column=catenation number
#Second column=integration probability in the braided part
#Third column=integration probability in the plectonemic part
#Fourth column=integration probability in the plectonemic tips

30  0.895527  1.79404  6.12245
31  0.952557  0.890357  3.54839
32  0.753512  1.39401  7.48503
33  0.863512  0.994778  5.42857
34  1.01025  0.750595  2.9148
35  1.04945  0.625728  3.2967
36  1.06169  0.489458  4.37498
37  0.799977  0.877224  6.73077
38  1.34599  0.496829  2.26667
