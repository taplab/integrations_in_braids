This repository contains the raw data to reproduce the figures in 
"Investigating Site-Selection Mechanisms of Retroviral Integration in Supercoiled DNA Braids" by 
Giada Forte et al and published in the Journal of Royal Society Interface. 

https://royalsocietypublishing.org/doi/10.1098/rsif.2021.0229

The folder lammps_code_exmaple contains some example codes to reproduce the simulations performed in our work. 

Enjoy! 
